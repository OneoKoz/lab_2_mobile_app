//
//  MainTableViewCell.swift
//  Draft_2_2
//
//  Created by иван on 11.03.22.
//

import UIKit

class MainTableViewCell: UITableViewCell {


    @IBOutlet weak var prodImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    

}
