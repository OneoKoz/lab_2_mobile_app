//
//  PersonalInfoViewController.swift
//  Draft_2_2
//
//  Created by иван on 11.03.22.
//

import UIKit
import MapKit

class PersonalInfoViewController: UIViewController {

    @IBOutlet weak var personMapView: MKMapView!
    override func viewDidLoad() {
        super.viewDidLoad()
        personMapView.showsUserLocation = true
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
