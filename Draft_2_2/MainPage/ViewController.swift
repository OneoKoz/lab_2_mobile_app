//
//  ViewController.swift
//  Draft_2_2
//
//  Created by иван on 22.02.22.
//

import UIKit
import SDWebImage

class ViewController: UIViewController, UISearchResultsUpdating  {
    
    @IBOutlet var prodTable: UITableView!
    
    @IBOutlet weak var currencyPickerView: UIPickerView!
    var products: [Product] = []
    var filteredProducts: [Product] = []
    var isNetWork = true
    var currency: [Currency] = []
    var temp_curr: Double = 1
    
    let searchController = UISearchController(searchResultsController: nil)
    
    func updateSearchResults(for searchController: UISearchController) {
        if searchController.searchBar.text! == ""{
            filteredProducts = products
        } else {
            filteredProducts = products.filter ({ item in  item.title.lowercased().contains(searchController.searchBar.text!.lowercased()) ||
                item.category.lowercased().contains(searchController.searchBar.text!.lowercased()) ||
                String(item.price).lowercased().contains(searchController.searchBar.text!.lowercased())
            })
        }
        self.prodTable.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filteredProducts = products
        searchController.searchResultsUpdater = self
        
        definesPresentationContext = true
        prodTable.tableHeaderView = searchController.searchBar
        
        scheduledTimerWithTimeInterval()
        
        if Reachability.isConnectedToNetwork() {
            let r = URLSession.shared.dataTask(with: URL(string: "https://fakestoreapi.com/products")!) { data, response, error in
                UserDefaults.standard.set(data!, forKey: "products")
                
                if let products = try? JSONDecoder().decode([Product].self, from: data!) {
                    
                    self.products = products
                    self.filteredProducts = self.products
                }
                
                DispatchQueue.main.async {
                    self.prodTable.reloadData()
                }
                
            }
            r.resume()
            let c = URLSession.shared.dataTask(with: URL(string: "https://www.nbrb.by/api/exrates/rates/451")!) {data, response, error in
                UserDefaults.standard.set(data!, forKey: "451")
                
                if let currency_temp = try? JSONDecoder().decode(Currency.self, from: data!) {
                    
                    self.currency.append(Currency(Cur_ID: 1, Date: "now", Cur_Abbreviation: "BYN", Cur_OfficialRate: 1))
                    print(self.currency)
                    self.currency.append (currency_temp)
                }
                DispatchQueue.main.async {
                    self.currencyPickerView.reloadAllComponents()
                }
            }
            c.resume()
        } else {
            guard let data = UserDefaults.standard.data(forKey: "products") else { return }
            
            if let products = try? JSONDecoder().decode([Product].self, from: data) {
                self.products = products
                self.filteredProducts = self.products
            }
            
            DispatchQueue.main.async {
                self.prodTable.reloadData()
            }
        }
        
    }
    
    var timer = Timer()
    
    func scheduledTimerWithTimeInterval(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.testNetworkConnection), userInfo: nil, repeats: true)
    }

    @objc func testNetworkConnection() {
        if isNetWork != Reachability.isConnectedToNetwork(){
            isNetWork = !isNetWork
            let text = Reachability.isConnectedToNetwork() ? "Есть интернет соединение" : "Нет интернет соединения"
            let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
            present(alert, animated: true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                alert.dismiss(animated: true, completion: nil)
            }
        }
    }

}

extension ViewController:UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.currency.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return currency[row].Cur_Abbreviation
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        for i in 0...filteredProducts.count-1{
            filteredProducts[i].price = filteredProducts[i].price * temp_curr / currency[row].Cur_OfficialRate
        }
        temp_curr = currency[row].Cur_OfficialRate
        
        self.prodTable.reloadData()
    }

}

extension ViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredProducts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let theCell = tableView.dequeueReusableCell(withIdentifier: "cellProd") as! MainTableViewCell

        let product = filteredProducts[indexPath.row]

        theCell.nameLabel.text = product.title
        theCell.categoryLabel.text = product.category
        theCell.priceLabel.text = String(product.price)
        theCell.prodImage.sd_setImage(with: URL(string: product.image), placeholderImage: UIImage(named: "placeholder"))

        return theCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let storyboardVC = storyboard.instantiateViewController(withIdentifier: "MoreInfo")
        (storyboardVC as? MoreInfoViewController)?.product = products[indexPath.row]
        present(storyboardVC, animated: true)
    }
}
