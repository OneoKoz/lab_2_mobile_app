//
//  Currency.swift
//  Draft_2_2
//
//  Created by иван on 11.03.22.
//

import Foundation
import UIKit

struct Currency: Codable {
    let Cur_ID: Int
    let Date: String
    let Cur_Abbreviation: String
    let Cur_OfficialRate: Double
}

