//
//  Product.swift
//  Draft_2_2
//
//  Created by иван on 11.03.22.
//

import Foundation
import UIKit

struct Product: Codable {
    let id: Int
    let title: String
    var price: Double
    let description: String
    let category: String
    let image: String
    let rating: Rating
}

struct Rating: Codable {
    let rate: Double
    let count: Int
}

